CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers

INTRODUCTION
-----------
This module helps you to hide specific field value for a particular node.

Uses:

1. This module makes easier to hide the field value by just typing 
the machine name of the field.

2. The Field Value is just hidden, You can reenable the field value 
easily by removing the value.

Note: This module is very useful to hide the values without any code writing.

REQUIREMENTS
------------
This project does not require any support modules.

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 * Does not require any configuration. Each node will have an option 
 to hide the field value.

TROUBLESHOOTING
---------------
 * If the Field value does not hide, check the following:
  - Immediately create a ticket in drupal.org 
  (https://www.drupal.org/node/add/project-issue/hide_node_field)
  - Clear the drupal cache if the value is not displayed.

MAINTAINERS
-----------
Current maintainers:
Aaron23 https://www.drupal.org/u/aaron23
